#!/usr/bin/env bash
#==========================================================
# Autor:          Hackspoiler
# URL:            https://hackspoiler.de
# Scriptname:     sys-monit-top-processes.sh
# Scriptpath:     /root/server-scripts/
# Usage:          /root/server-scripts/sys-monit-top-processes.sh
# Version:        0.5
# Date:           16.06.2020
# Shellcheck:     True
# Package:        mail
# Modification:   06.09.2024
# Descritpion:    Monit Skript das bei Alerts den Hostname, die Load Average und die Top 10 CPU/RAM-Verschwendert verpetzt
#==========================================================

# Set Bash-Defaults
#==========================================================
set -o errexit  # Beenden, falls ein Befehl fehlschlägt (-e)
set -o nounset  # Beenden, falls ungesetzte Variable exisitiert (-u)
set -o pipefail # Beenden, falls eine Pipeline fehlschlägt

# Set Variables
#==========================================================
EMAIL_SUBJECT="Top Prozesse"
EMAIL_ADDRESS="server@${DOMAIN_NAME}"

# Main
#==========================================================

# Systeminformationen
FULL_HOSTNAME=$(hostname --fqdn)
LOAD_AVERAGE=$(uptime | awk -F 'load average: ' '{print $2}')

# Prozessinformationen
CPU_OUTPUT=$(ps aux --sort=-%cpu | head --lines 11)
MEM_OUTPUT=$(ps aux --sort=-%mem | head --lines 11)

# Email-Inhalt
EMAIL_BODY="# FQDN des Systems
${FULL_HOSTNAME}

# Load Average des Systems
${LOAD_AVERAGE}

# Top 10 Prozesse nach CPU-Auslastung:
${CPU_OUTPUT}

# Top 10 Prozesse nach Speicherverbrauch:
${MEM_OUTPUT}"

# Emailversand
echo -e "${EMAIL_BODY}" | mail -s "${EMAIL_SUBJECT}" "${EMAIL_ADDRESS}"