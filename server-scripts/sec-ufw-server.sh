#!/usr/bin/env bash
#==========================================================
# Autor:          Hackspoiler
# URL:            https://hackspoiler.de
# Scriptname:     sec-ufw-server.sh
# Scriptpath:     /root/server-scripts/
# Usage:          sec-ufw-server.sh
# Version:        1.0
# Date:           03.02.2021
# Shellcheck:     True
# Package:        ufw
# Modification:   09.04.2023
# Descritpion:    Debian based OG Firewall for a sexy Server Security. Custom SSH-Port is 63007
#==========================================================

# Set Bash-Defaults
set -o errexit  # Beenden, falls ein Befehl fehlschlägt
set -o nounset  # Beenden, falls ungesetzte Variable geused wird
set -o pipefail # Beenden, falls eine Pipeline fehlschlägt

# Set les Variablés
readonly UFW="$(command -v ufw)"
readonly IPFW="$(command -v iptables)"
readonly IPFW_IPV6="$(command -v ip6tables)"

# Activate UFW on startup
sed -i '/ENABLED=no/c\ENABLED=yes' /etc/ufw/ufw.conf

# Deactivate IPv6 if nix needed
sed -i '/IPV6=yes/c\IPV6=no' /etc/default/ufw
 
# Reset Feuerwand-Settings
"${UFW}" --force reset
"${IPFW}" -F
"${IPFW}" -X
"${IPFW_IPV6}" -F
"${IPFW_IPV6}" -X

# Activate Logging
"${UFW}" logging low

# Set stabile Firewall-Policies
"${UFW}" default deny incoming
"${UFW}" default deny outgoing
"${UFW}" default deny forward

# Limit custom SSH-Connections.
"${UFW}" limit 60007/tcp comment 'SSH-Port Rate Limit'

# Set da custom Firewall-Rules (tcp)
"${UFW}" allow in 25,53,80,123,143,443,465,587,993,60007/tcp comment 'Standard Incomming Ports'
"${UFW}" allow out 22,25,53,80,123,143,443,587,993,60007/tcp comment 'Standard Outgoing Ports'

# Set da custom Firewall-Rules (udp)
"${UFW}" allow in 53,123/udp comment  'Allow NTP and DNS in'
"${UFW}" allow out 53,123/udp comment 'Allow NTP and DNS out'

# Enable this super-crispy Firewall
"${UFW}" --force enable

#------------------------------------------
# Documentation
#------------------------------------------

# Default Rules/Policy
#------------------------------------------
# /etc/ufw/before.rules
# /var/lib/ufw/user.rules
# /etc/ufw/after.rules
 
# Default Policy: deny (incoming), allow (outgoing), deny (routing)

# Used Ports
#------------------------------------------
# OpenSSH               TCP         22
# OpenSSH Custom        TCP         60007

# DNS-Server            TCP/UDP     53
# HTTP(S)               TCP         80/443
# NTP                   TCP/UDP     123

# Postfix SMTP          TCP         25
# Postfix SMTPS         TCP         465
# Postfix Submission    TCP         587
# Dovecot IMAP          TCP         143
# Dovecot IMAPS         TCP         993

#https://de.wikipedia.org/wiki/Liste_der_standardisierten_Ports

#------------------------------------------
# Common UFW Commands
#------------------------------------------

# UFW Control
#------------------------------------------
# ufw (enable|disable|reset)

# Set a strong Firewall-Policy
#------------------------------------------
# ufw default deny

# Set da fresh Logging
#------------------------------------------
# ufw logging (off|low|medium|high|full)

# Show Infos
#------------------------------------------
# ufw show (raw|listening)
# ufw status (verbose|numbered)

# Delet a special Rule
#------------------------------------------
# ufw delete $NUMBER
# ufw delete deny 80/tcp

# Allow special Port-Range
#------------------------------------------
# ufw allow 3000:3005/tcp  comment 'Allow Port-Range'

# Allow $IP for SSH-Sessions
#------------------------------------------
# ufw allow from $IP to any port 63007 comment 'Subnet - FFM'

# Whitelist a $IP - Its important to set it above the deny-Rules
#------------------------------------------
# ufw insert 1 allow from $IP comment 'Whitelist $IP'

# Allow a $IP-Subnet to a special Port
#------------------------------------------
# ufw allow from $IP/$CIDR to any port 63007 comment 'Subnet - FFM'

# Limit Port/Connection against Brute-Force-Attacks. Customize the Rate Limit in /etc/ufw/user.rules
#------------------------------------------
# ufw limit $PORT/$PROTOCOL comment 'Rate Limit Port/$PROTOCOL'

# Show da UFW-Rule Result before apply dem roughly
#------------------------------------------
# ufw --dry-run
# ufw --dry-run limit ssh

# Aktuelle Rules anzeigen
#------------------------------------------
# ufw show listening
# ufw show added