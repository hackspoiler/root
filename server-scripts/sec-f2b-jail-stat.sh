#!/usr/bin/env bash
#============================================================
# Autor:        Hackspoiler
# URL:          https://hackspoiler.de
# Datum:        02.04.2019
# Version:      1.0
# Shellcheck:   True
# Scriptname:   sec-f2b-jail-stat.sh
# Scriptpath:   /root/server-scripts/
# Packages:     fail2ban
# Description:  Show Fail2ban status from used Jails
#============================================================

# Debug-Funktion
# set -x

# Set Bash-Defaults
set -eou pipefail

# Variablen
FAIL2BAN_CLIENT="$(which fail2ban-client)"

"${FAIL2BAN_CLIENT}" status | awk -F ':' '/Jail list/ {print $2}' | tr ',' '\n' | while IFS= read -r JAIL
do
    JAIL=$(echo ${JAIL} | xargs)  # remove leading whitespaces
    echo "# ================= Jail ${JAIL}  ================= #"
    "${FAIL2BAN_CLIENT}" status "${JAIL}" | grep -v -E 'Status for the jail|File list'
    echo ""
done