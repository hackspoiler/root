#!/usr/bin/env bash
#==========================================================
# Autor:          Hackspoiler
# URL:            https://hackspoiler.de
# Scriptname:     sec-netguard-blocklist.sh
# Scriptpath:     /root/server-scripts/
# Usage:          sec-netguard-blocklist.sh
# Version:        1.0
# Date:           19.02.2020
# Shellcheck:     True
# Package:        curl
# Modification:   06.04.2023
# Descritpion:    Auto download multiple Blocklist-Files, combine sem in one File (netguard.txt) to rule dem bitches all
#==========================================================

# Set Bash-Defaults
set -o errexit  # Beenden, falls ein Befehl fehlschlägt
set -o nounset  # Beenden, falls ein ungesetzter Variablenname verwendet wird
set -o pipefail # Beenden, falls eine Pipeline fehlschlägt

# Variables
readonly DOMAIN_NAME="hackspoiler.de"
readonly NETGUARD_FINAL_FILE="netguard.txt"
readonly FILTERLIST_DL_DIR="/tmp"
readonly FILTERLIST_UL_DIR="/srv/vhosts/${DOMAIN_NAME}/live/pub"

# Function - Download & Filter handverlesener Open-Source Blocklisten
dl_and_filter_blocklist() {
  local BLOCKLIST_URL="${1}"
  local BLOCKLIST_FILE_NAME="${2}"
  curl --silent "${BLOCKLIST_URL}" | sed -e '/^#/d' -e '/^$/d' -e '/^::1/d' -e '/^ff/d' -e '/fe80/d' > "${BLOCKLIST_FILE_NAME}"
}

# Download der Open-Source Blocklists
dl_and_filter_blocklist "https://blocklistproject.github.io/Lists/ads.txt" "${FILTERLIST_DL_DIR}/netguard_blocklist.txt"
dl_and_filter_blocklist "https://raw.githubusercontent.com/badmojr/1Hosts/master/Pro/hosts.txt" "${FILTERLIST_DL_DIR}/netguard_1Hosts.txt"
dl_and_filter_blocklist "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts" "${FILTERLIST_DL_DIR}/netguard_stevenblack.txt"

# Blocklisten zusammenführen
cat "${FILTERLIST_DL_DIR}/netguard_blocklist.txt" "${FILTERLIST_DL_DIR}/netguard_1Hosts.txt" "${FILTERLIST_DL_DIR}/netguard_stevenblack.txt" > "${FILTERLIST_DL_DIR}/netguard_main.txt"

# Doubletten aus Blockliste rausballern
sort --unique "${FILTERLIST_DL_DIR}/netguard_main.txt" > "${FILTERLIST_DL_DIR}/${NETGUARD_FINAL_FILE}"

# Blockliste auf Webspace schieben
cp "${FILTERLIST_DL_DIR}/${NETGUARD_FINAL_FILE}" "${FILTERLIST_UL_DIR}"

# Berechtigung anpassen (Nur lesen)
chmod 444 "${FILTERLIST_UL_DIR}/${NETGUARD_FINAL_FILE}"