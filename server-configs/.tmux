#==========================================================
# Default
#==========================================================
set -g default-terminal "screen-256color"
set -ga terminal-overrides ",*256col*:Tc"
if 'infocmp -x tmux-256color > /dev/null 2>&1' 'set -g default-terminal "tmux-256color"'

set -g mouse on
setw -q -g utf8 on
set -q -g status-utf8 on

set -g history-limit 20000

setw -g xterm-keys on
set -s escape-time 10
set -sg repeat-time 600
set -s focus-events on

# Reload config
unbind r
bind r source-file ~/.tmux.conf \; display-message "~/.tmux.conf sourced && Tmux config Reloaded."

#==========================================================
# Binds
#==========================================================
# Remap prefix from 'C-b' to 'C-a'
unbind C-b
set -g prefix C-a

setw -g mode-keys vi

# Split Window
unbind '"'
unbind %
bind v split-window -h
bind h split-window -v

# Navigation-Binds
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D
bind > swap-pane -D              # Swap current pane with the next one
bind < swap-pane -U              # Swap current pane with the previous one

# Kill Window/Pane
bind x kill-pane
bind X kill-window
bind C-x confirm-before -p "Kill other windows? (y/n)" "kill-window -a"
bind Q confirm-before -p "Kill-session #S? (y/n)" kill-session

# Sessions
bind C-c new-session                                          # Create session
bind C-f command-prompt -p find-session 'switch-client -t %%' # Find session
bind BTab switch-client -l                                    # Move to last session

# Detach from session
bind d detach
bind D if -F '#{session_many_attached}' \
    'confirm-before -p "Detach other clients? (y/n)" "detach -a"' \
    'display "Session has only 1 client attached"'

# Copy-Binds
bind -T copy-mode    C-c send -X copy-pipe-no-clear "xsel -i --clipboard"
bind -T copy-mode-vi C-c send -X copy-pipe-no-clear "xsel -i --clipboard"

#==========================================================
# Window
#==========================================================
# Activity
set -g monitor-activity on
set -g visual-activity off

set -g set-titles on          # set terminal title
set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows

setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed

set -g display-panes-time 800 # slightly longer pane indicators display time
set -g display-time 1000      # slightly longer status messages display time

#==========================================================
# Status bar styling
#==========================================================
set -g status on
set -g status-keys vi
set -g status-interval 1
set -g status-position bottom
set -g status-justify left

set -g status-style bold
set -g status-bg colour232
set -g status-fg colour15
set -g window-status-current-style bg=colour15
set -g window-status-current-style fg=colour0
set -g window-status-current-style bold

# Status Left
set -g status-left-length 30
set -g status-left-style default
set -g status-left "#[fg=colour75] Hostname: #[fg=colour15]#H "

# Status Right
set -g status-right-length 80
set -g status-right-style default
set -g status-right '#[fg=colour167,bg=colour0] CPU: #(cut -d " " -f 1-3 /proc/loadavg) #[fg=colour235,bg=colour167] User: #(/usr/bin/whoami) #[fg=colour75,bg=colour232] %Y-%m-%d %H:%M:%S '

# Colors (https://www.ditig.com/256-colors-cheat-sheet)
# colour0=Black | colour15=White | colour75=SteelBlue1 | colour167=IndianRed | colour232=Grey3 | colour235=Grey15

#==========================================================
# Plugins
#==========================================================
# List of plugins
#set -g @plugin 'tmux-plugins/tpm'
#set -g @plugin 'tmux-plugins/tmux-sensible'
#set -g @plugin 'tmux-plugins/tmux-yank'
#set -g @yank_action 'copy-pipe-no-clear'

# Reload plugins
#run '~/.tmux/plugins/tpm/tpm'