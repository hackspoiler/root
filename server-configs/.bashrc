#============================================================
# Defaults
#============================================================
# Default Umask setzen
umask 027

# Core dumps deaktivieren
ulimit -c 0

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# Standard-Pager (man-Pager)
PAGER="/usr/bin/most -s"

#============================================================
# Shopts
#============================================================
shopt -s histappend
shopt -s checkwinsize

#===========================================================
# History
#===========================================================
# Sec - History
HISTSIZE=30000
HISTFILESIZE=30000
HISTCONTROL=ignoredups
HISTIGNORE="ls:cd:ll:history:clear:htpasswd*"

#===========================================================
# Prompt
#===========================================================
if [[ "$TERM" == "xterm-color" ]]; then
    color_prompt="yes"
fi

force_color_prompt="yes"

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
  color_prompt="yes"
    else
  color_prompt="no"
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='\[\e[0m\][\[\e[0;38;5;161m\]$?\[\e[0m\]] \[\e[0m\]\A \[\e[0;1;91m\]\u\[\e[0m\]@\[\e[0;1m\]\h\[\e[0m\]:\[\e[0m\]\w\[\e[0m\]/ \[\e[0m\]'
    # Live PS1='\[\e[0m\][\[\e[0;38;5;161m\]$?\[\e[0m\]] \[\e[0m\]\A \[\e[0;1;91m\]\u\[\e[0m\]@\[\e[0;1m\]\h\[\e[0m\]:\[\e[0m\]\w\[\e[0m\]/ \[\e[0m\]'
    # Dev PS1='\[\e[0m\][\[\e[0;38;5;161m\]$?\[\e[0m\]] \[\e[0m\]\A \[\e[0m\]\u\[\e[0m\]@\[\e[0;1;38;5;115m\]\h\[\e[0m\]:\[\e[0m\]\w\[\e[0m\]/ \[\e[0m\]'
    # Stage PS1='\[\e[0m\][\[\e[0;38;5;161m\]$?\[\e[0m\]] \[\e[0m\]\A \[\e[0m\]\u\[\e[0m\]@\[\e[0;1;38;5;214m\]\h\[\e[0m\]:\[\e[0m\]\w\[\e[0m\]/ \[\e[0m\]'
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

#===========================================================
# Includes
#===========================================================
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

#===========================================================
# Exports
#===========================================================
# Editor des Vertrauens
export EDITOR=/usr/bin/vim

# Coloring
export TERM="xterm-256color"

# Skriptpfad in PATH-Variable erweitern
export PATH="$PATH:/root/server-scripts"

