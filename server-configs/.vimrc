"============================================================
" Autor:        Hackspoiler
" URL:          https://hackspoiler.de
" Date:         11.11.2020
" Version:      1.0
" Packages:     vim vim-plug
" Description:  Sexy vim configuration
"============================================================ 

"============================================================
" Plugin-Manager installen
"============================================================
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
 
"============================================================
" Grundeinstellungen
"============================================================
syntax on
"set list

set sm
set scs

set nocompatible
 
set magic
set autoread
set showmatch
set mat=2

set nowb
set nocp
set paste
set nobackup
set guicursor=
set scrolloff=8
set laststatus=2
 
set showcmd
set showmode
set noswapfile
set cursorline
set cursorcolumn
 
set expandtab
set smarttab
set tabstop=2
set shiftwidth=2
set softtabstop=2

set lazyredraw
set history=500
set undolevels=500
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
 
set hlsearch
set incsearch
set ignorecase
set smartcase
set ffs=unix
 
set formatoptions+=j
 
let mapleader = ","
 
"============================================================
" Plugins
"============================================================
filetype plugin on
filetype indent on

call plug#begin('~/.vim/plugged')
  Plug 'scrooloose/nerdtree'
  Plug 'joshdick/onedark.vim'
  Plug 'vim-airline/vim-airline'
  Plug 'pearofducks/ansible-vim'
  Plug 'scrooloose/nerdcommenter'
call plug#end()
 
"============================================================
" Plugin Einstellungen
"============================================================
color onedark
 
"============================================================
" Mappings
"============================================================
map <C-t> :NERDTreeToggle<CR>
noremap <F5> :set list!<CR>
inoremap <F5> <C-o>:set list!<CR>
cnoremap <F5> <C-c>:set list!<CR>
