#!/bin/bash
# Autor: Hackspoiler
# Version: 1.1
# Skriptname: sec-inventory.sh
# Skriptpfad: /root/scripts/
# Pakete: acl dstat dmidecode dnsutils gnupg2 lynis nmap osquery pigz psmisc rng-tools ufw
# Glossar: net=Network, osq=Osquery, perf=Performance, sec=Security, sys=Systeminfos
# Beschreibung: Erstellt eine Inventarisierung (Baselining) eines Linux Servers

# Check for root permissions
if [[ "$(id -u)" -ne 0 ]]; then
  echo "Requires root permissions" > /dev/stderr
  exit 1
fi

# Set Bash-Defaults
set -uo pipefail
BACKIFS="$IFS"
IFS=$'\n\t'

#---------------------------------------------
# Grundeinstellungen
#---------------------------------------------

# Set les Variablés
LOGFILE="/var/log/syslog"
TIMESTAMP=$(date +'%Y-%m-%d-%H-%M-%S')
INVENT_DIR="/root/inventory/${TIMESTAMP}"
BACKUP_DIR="/var/backups/inventory/${TIMESTAMP}"

# Optionen je nach Ressourcen anpassen
# -P = Zu verfügung gestellte Prozessoren
XARGS=('xargs' '-P2')

## Logging starten
exec > >(tee -i "${LOGFILE}")
exec 2>&1

# Ordnerstruktur anlegen
mkdir -p "${INVENT_DIR}"
mkdir -p "${BACKUP_DIR}"

# Berechtigung brav setzen
find "${INVENT_DIR}" -type d | "${XARGS[@]}" chmod -v 700
find "${BACKUP_DIR}" -type d | "${XARGS[@]}" chmod -v 700

#---------------------------------------------
# Datei-Indexierung auf dem System
#---------------------------------------------
file_index_check () {
    echo "# Inventory - Datei-Indexierung auf dem System startet #"
    ls -la /* > "${INVENT_DIR}"/sys-list-root-files.txt
    find / ! -path "/mnt/*" ! -path "/proc/*" -ls > "${INVENT_DIR}"/sys-list-all-files.txt
}

#---------------------------------------------
# System-Infos
#---------------------------------------------
system_check () {
    echo "# Inventory - Die Hocherotische Systeminfo-Collection startet #"
    free -m > "${INVENT_DIR}"/sys-memory.txt
    lastlog > "${INVENT_DIR}"/sys-lastlog.txt
    df -h > "${INVENT_DIR}"/sys-diskspace.txt
    ps axjf > "${INVENT_DIR}"/sys-processlist.txt
    hostnamectl > "${INVENT_DIR}"/sys-hostinfo.txt
    cat /etc/group > "${INVENT_DIR}"/sys-group.txt
    cat /etc/fstab > "${INVENT_DIR}"/sys-fstab.txt
    cat /etc/passwd > "${INVENT_DIR}"/sys-user.txt
    dpkg -l > "${INVENT_DIR}"/sys-deb-packages.txt
    who -a > "${INVENT_DIR}"/sys-logged-in-users.txt
    uname -a > "${INVENT_DIR}"/sys-kernelversion.txt
    dmidecode -t 4 > "${INVENT_DIR}"/sys-cpu-info.txt
    pstree -a  > "${INVENT_DIR}"/sys-process-pstree.txt
    du -sh --exclude=/proc /* > "${INVENT_DIR}"/sys-dirspace.txt
    cat /root/.bash_history > "${INVENT_DIR}"/sys-root-history.txt
    sysctl -A > "${INVENT_DIR}"/sys-sysctl-settings.txt 2> /dev/null
    egrep -v "^\s*(#|$)" /var/spool/cron/crontabs/* > "${INVENT_DIR}"/sys-crontabs.txt
    systemctl --type=service --state=active > "${INVENT_DIR}"/sys-systemctl-active-services.txt
    systemctl list-dependencies graphical.target > "${INVENT_DIR}"/sys-systemctl-services-tree.txt
    systemctl list-units --all --type=service --no-pager > "${INVENT_DIR}"/sys-systemctl-services.txt
    lsblk -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINT,UUID,OWNER,GROUP,MODE,RO,MODEL,STATE > "${INVENT_DIR}"/sys-disks.txt
    find / -type f ! -path "/mnt/*" ! -path "/proc/*" -size +500M 2> /dev/null | "${XARGS[@]}" -d '\n' ls -lah > "${INVENT_DIR}"/sys-big-files.txt
}

#---------------------------------------------
# Netzwerk-Infos
#---------------------------------------------
network_check () {
    echo "# Inventory - Netzwerkinfo-Collection startet #"
    ss -ltp > "${INVENT_DIR}"/net-ss-stat.txt
    ip -br -c addr show > "${INVENT_DIR}"/net-ip-addr.txt
    ip -c -d addr show >> "${INVENT_DIR}"/net-ip-addr.txt
    ip -br -c route show > "${INVENT_DIR}"/net-route.txt
    lsof -i > "${INVENT_DIR}"/net-lsof-stat-networkresources.txt
}

#---------------------------------------------
# Security-Infos
#---------------------------------------------
security_check () {
    echo "# Inventory - Securityinfo-Collection startet #"
    umask > "${INVENT_DIR}"/sec-umask.txt
    cat /etc/sudoers > "${INVENT_DIR}"/sec-sudoers.txt
    ufw status verbose > "${INVENT_DIR}"/sec-ufw-firewall.txt
    getfacl -R / > "${INVENT_DIR}"/sec-getfacl-permissions.acl 2> /dev/null
    nmap -p- -T4 -sS localhost --max-retries 1 > "${INVENT_DIR}"/sec-nmap-portscan.txt
    lynis audit system --quick --quiet --auditor "The Boss" --logfile "${INVENT_DIR}"/sec-lynis.txt
    cat /root/.ssh/{known_hosts,authorized_keys} > "${INVENT_DIR}"/sec-root-ssh-keys.txt 2> /dev/null
    find /etc -type f -print0 | "${XARGS[@]}" -0 sha256sum > "${INVENT_DIR}"/sec-sha256sum-etc.txt
    find /usr -type f -print0 | "${XARGS[@]}" -0 sha256sum > "${INVENT_DIR}"/sec-sha256sum-usr.txt
    find / -nouser -o -nogroup ! -path "/mnt/*" ! -path "/proc/*" -ls > "${INVENT_DIR}"/sec-nouser-nogroup.txt 2> /dev/null
    find / -perm -2 ! -type l ! -type c ! -type b ! -path "/mnt/*" ! -path "/proc/*" -ls > "${INVENT_DIR}"/sec-world-writable.txt 2> /dev/null
    for DIR in bin boot dev etc home lib lib64 opt root sbin srv usr var; do getfacl -p -R "/${DIR}" > "${INVENT_DIR}"/sec-getfacl-permissions-"${DIR}".acl; done
}

#---------------------------------------------
# Performance-Infos
#---------------------------------------------
performance_check () {
    echo "# Inventory - Performance-Collection startet #"
    dstat --top-cpu-adv --top-latency --top-io --top-mem --mem --load --time 1 20 > "${INVENT_DIR}"/perf-dstat.txt
    dd if=/dev/zero of="${BACKUP_DIR}"/testfile bs=512 count=1000 oflag=dsync > "${INVENT_DIR}"/perf-dd-disk-speed.txt && rm "${BACKUP_DIR}"/testfile
    dd if=/dev/zero of="${BACKUP_DIR}"/testfile bs=1MB count=1 oflag=dsync >> "${INVENT_DIR}"/perf-dd-disk-speed.txt && rm "${BACKUP_DIR}"/testfile
    dd if=/dev/zero of="${BACKUP_DIR}"/testfile bs=100MB count=1 oflag=dsync >> "${INVENT_DIR}"/perf-dd-disk-speed.txt && rm "${BACKUP_DIR}"/testfile
    dd if=/dev/zero of="${BACKUP_DIR}"/testfile bs=500MB count=1 oflag=dsync >> "${INVENT_DIR}"/perf-dd-disk-speed.txt && rm "${BACKUP_DIR}"/testfile
}

#---------------------------------------------
# Osquery-Abfragen
#---------------------------------------------
OSQUERY="$(which osqueryi)"

osquery_check () {
    echo "# Inventory - Osquery-Collection startet #"

    # Osquery - Config check
    osqueryi "select config_hash, config_valid from osquery_info;" > "${INVENT_DIR}"/osq-configcheck.txt

    # Osquery - Systeminfos
    osqueryi "SELECT * FROM memory_info;" > "${INVENT_DIR}"/osq-sys-memory.txt
    osqueryi "SELECT * FROM os_version;" > "${INVENT_DIR}"/osq-sys-osversion.txt
    osqueryi "SELECT count(*) FROM crontab;" > "${INVENT_DIR}"/osq-sys-crontabs.txt
    osqueryi "SELECT command, path FROM crontab;" >> "${INVENT_DIR}"/osq-sys-crontabs.txt
    osqueryi "SELECT days, datetime(time.local_time - uptime.total_seconds, 'unixepoch') AS last_rebooted FROM time, uptime;" > "${INVENT_DIR}"/osq-sys-uptime.txt
    osqueryi "SELECT hostname, cpu_brand, cpu_physical_cores, cpu_logical_cores, computer_name, physical_memory FROM system_info;" > "${INVENT_DIR}"/osq-sys-hardware.txt
    osqueryi "SELECT pid, name, ROUND((total_size * '10e-7'), 2) AS used FROM processes ORDER BY total_size DESC LIMIT 10;" > "${INVENT_DIR}"/osq-sys-memory-intensive-processes.txt
    osqueryi "SELECT pid, name, path, cmdline, state, cwd, root, uid, gid, suid, sgid, on_disk, total_size, user_time, system_time, start_time, parent, pgroup, threads, nice FROM processes LIMIT 20;" >> "${INVENT_DIR}"/osq-sys-memory-intensive-processes.txt
    osqueryi "SELECT device, path, type, blocks_size, blocks, blocks_free, blocks_available, inodes, inodes_free, flags FROM mounts where type!='cgroup' and type!='tmpfs' and inodes!='0';" > "${INVENT_DIR}"/osq-sys-devices.txt
    osqueryi "SELECT device, device_alias, path, blocks, blocks_free, inodes, inodes_free, flags, encrypted, encryption_status FROM mounts m, disk_encryption d WHERE m.device_alias = d.name;" > "${INVENT_DIR}"/osq-sys-devices-encrypted.txt
    osqueryi "SELECT pid, uid, name, ROUND((  (user_time + system_time) / (cpu_time.tsb - cpu_time.itsb)) * 100, 2) AS percentage FROM processes, (SELECT (SUM(user) + SUM(nice) + SUM(system) + SUM(idle) * 1.0)
                    AS tsb, SUM(COALESCE(idle, 0)) + SUM(COALESCE(iowait, 0)) AS itsb FROM cpu_time) AS cpu_time ORDER BY user_time+system_time DESC LIMIT 20;" > "${INVENT_DIR}"/osq-sys-cpu-intensive-processes.txt

    # Osquery - Paketmanagement
    osqueryi "SELECT count(*) FROM deb_packages;" > "${INVENT_DIR}"/osq-sys-deb-packages.txt
    osqueryi "SELECT name, version, source, size, arch, revision, status FROM deb_packages;" >> "${INVENT_DIR}"/osq-sys-deb-packages.txt
    osqueryi "SELECT name, base_uri, release, maintainer, components FROM apt_sources ORDER BY name;" > "${INVENT_DIR}"/osq-sys-deb-sources.txt

    # Osquery - Usermanagement
    osqueryi "SELECT * FROM last;" > "${INVENT_DIR}"/osq-sys-login-users.txt
    osqueryi "SELECT * FROM logged_in_users;" > "${INVENT_DIR}"/osq-sys-loggedin-users.txt
    osqueryi "SELECT * FROM users WHERE gid < 65534 AND uid >= 1000;" > "${INVENT_DIR}"/osq-sys-non-system-users.txt
    osqueryi "SELECT count(*) FROM users" > "${INVENT_DIR}"/osq-sys-all-users.txt
    osqueryi "SELECT uid, gid, uid_signed, gid_signed, username, description, directory, shell FROM users;" >> "${INVENT_DIR}"/osq-sys-all-users.txt

    # Osquery - Aktive Kernelmodule
    osqueryi "SELECT count(*) FROM kernel_modules WHERE status='Live';" > "${INVENT_DIR}"/osq-sys-kernelmodule.txt
    osqueryi "SELECT name, used_by, status FROM kernel_modules WHERE status='Live';" >> "${INVENT_DIR}"/osq-sys-kernelmodule.txt

    # Osquery - Securitymanagement
    osqueryi "SELECT count(*) FROM suid_bin;" > "${INVENT_DIR}"/osq-sec-setuid.txt
    osqueryi "SELECT * FROM suid_bin;" >> "${INVENT_DIR}"/osq-sec-setuid.txt
    osqueryi "SELECT count(*) FROM sudoers;" > "${INVENT_DIR}"/osq-sec-sudoers.txt
    osqueryi "SELECT * FROM sudoers;" >> "${INVENT_DIR}"/osq-sec-sudoers.txt
    osqueryi "SELECT count(*) FROM iptables;" > "${INVENT_DIR}"/osq-sec-firewall-all.txt
    osqueryi "SELECT * FROM iptables;" >> "${INVENT_DIR}"/osq-sec-firewall-all.txt
    osqueryi "SELECT chain, policy, src_ip, dst_ip FROM iptables;" > "${INVENT_DIR}"/osq-sec-firewall-short.txt
    osqueryi "SELECT suid_bin.path, username, groupname, permissions, hash.sha256 FROM hash JOIN suid_bin USING (path);" >> "${INVENT_DIR}"/osq-sec-setuid.txt

    # Osquery - Network
    osqueryi "SELECT count(*) FROM routes;" > "${INVENT_DIR}"/osq-net-routes.txt
    osqueryi "SELECT * FROM routes;" >> "${INVENT_DIR}"/osq-net-routes.txt
    osqueryi "SELECT * FROM etc_hosts;" > "${INVENT_DIR}"/osq-net-hosts.txt
    osqueryi "SELECT count(*) FROM listening_ports;" > "${INVENT_DIR}"/osq-net-listening-ports.txt
    osqueryi "SELECT DISTINCT processes.name, listening_ports.path, processes.pid, listening_ports.port, listening_ports.protocol, listening_ports.family, listening_ports.address, listening_ports.socket FROM listening_ports JOIN processes using (pid);" >> "${INVENT_DIR}"/osq-net-listening-ports.txt
    osqueryi "SELECT p.cmdline, pos.local_address, pos.remote_address, local_port, pos.remote_port from process_open_sockets as pos JOIN processes as p ON pos.pid=p.pid WHERE pos.state='ESTABLISHED';" > "${INVENT_DIR}"/osq-net-established-connections.txt

    # Osquery - Docker
    osqueryi "SELECT id, key, value FROM docker_image_labels;" > "${INVENT_DIR}"/osq-docker-images.txt
    osqueryi "SELECT name, id, os, cpus, memory, containers, containers_running, containers_paused, containers_stopped, images, storage_driver, server_version, root_dir FROM docker_info;" > "${INVENT_DIR}"/osq-docker-infos.txt
}

#---------------------------------------------
# Abschluss
#---------------------------------------------
abschluss () {
    # Checksum aus allen Dateien bilden
    echo "# Inventory - Checksum startet #"
    find "${INVENT_DIR}"/ -type f -print0 | "${XARGS[@]}" -0 sha256sum > "${INVENT_DIR}"/sec-sha256sum-inventory.txt

    # Inventory-Archiv inklusive wichtiger Systemdaten (Beispiel etc, usr) erstellen
    echo "# Inventory - Inventory-Archivierung startet #"
    tar --use-compress-program="pigz -k " -cf "${BACKUP_DIR}"/inventory-$(hostname -f)-"${TIMESTAMP}".tgz \
        /etc \
        /usr \
        "${INVENT_DIR}"/*

    # Inventory-Archiv verschlüsseln
    echo "# Inventory - Inventory-Archiv-Encryption startet #"
    cat /root/.inventory | /usr/bin/gpg --batch --yes --passphrase-fd 0 -c "${BACKUP_DIR}"/inventory-$(hostname -f)-"${TIMESTAMP}".tgz

    # Unverschlüsseltes Inventory-Archiv entfernen
    echo "# Inventory - Entfernung des einfachen Inventory-Archiv startet #"
    rm --force "${BACKUP_DIR}"/inventory-$(hostname -f)-"${TIMESTAMP}".tgz

    # Inventory-Archive älter als einen Tag rasieren
    echo "# Inventory - Entfernung alter Inventory-Archive startet #"
    find /root/inventory/ -mindepth 1 -type d -mtime +1 -print0 | xargs -0 -I {} rm --recursive --force {}
    find /var/backups/inventory/ -mindepth 1 -type d -mtime +1 -print0 | xargs -0 -I {} rm --recursive --force {}

    # Haben fertig
    echo "# Inventory - Inventory wurde in ${BACKUP_DIR} erstellt - Zeit für ein hochverdientes Bio-Bierchen #"
}

#---------------------------------------------
# Funktionsaufrufe
#---------------------------------------------
file_index_check
system_check
network_check
security_check
performance_check
[[ -n "${OSQUERY}" ]] && osquery_check
abschluss

# Set IFS back to default
IFS=$BACKIFS
